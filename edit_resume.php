<?php
include("fj-admin/config/confg.php");
include("session_check.php");
$user_id=$_SESSION['user_id'];

$user_resume = "SELECT * FROM job_seeker_resume WHERE user_id = '$user_id' ";
$user_resume_data = $con->query($user_resume);


$user_edu_info = "SELECT * FROM job_seeker_education WHERE user_id = '$user_id' ";
$user_edu_info_result = $con->query($user_edu_info);

$profess_info = "SELECT * FROM job_seeker_profess_info WHERE user_id = '$user_id' ";
$profess_info_result = $con->query($profess_info);



?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'head.php';?>

<link rel="stylesheet" href="css/neon-forms.css">

</head>
<body>
<?php include 'header-menu.php';?>




<div class="inner-content loginWrp">
  <div class="container">
    <div class="row">
      <?php include("user-sidebar-menu.php"); ?>
      


<div class="col-md-9"> 
  <!-- Blog List start -->
  <div class="user-content">
    <div class="panel-heading panel-heading-01"><i class="fa fa-pencil-square-o"></i> Edit Resume</div>

    <div class="ucon-panel-body">

     


     <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
           Personal Details 
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
         <form role="form" method="post" enctype="multipart/form-data" name="post"  action="edit_resume_ac.php">

<?php if($user_resume_view = $user_resume_data->fetch_assoc()){?>

<div class="input-wrap col-md-6">
                <input name="fathers_ame" placeholder="Your Father's Name" class="form-control" type="text" value="<?php echo $user_resume_view['fathers_ame'];?>">
              </div>

              <div class="input-wrap col-md-6">
                <input name="mother_name" placeholder="Your Mother's Name" class="form-control" type="text" value="<?php echo $user_resume_view['mother_name'];?>">
              </div>


              <div class="input-wrap col-md-6">
                <input name="birth_date" placeholder="Date of Birth" class="form-control" type="Date" value="<?php echo $user_resume_view['birth_date'];?>">
              </div>


              <div class="input-wrap col-md-6">
                <input name="religion" placeholder="Religion" class="form-control" type="text" value="<?php echo $user_resume_view['religion'];?>">
              </div>


               <div class="input-wrap col-md-6">
                <input name="marital_status" placeholder="Marital Status" class="form-control" type="text" value="<?php echo $user_resume_view['marital_status'];?>">
              </div>

              <div class="input-wrap col-md-6">
                <input name="nationality" placeholder="Nationality" class="form-control" type="text" value="<?php echo $user_resume_view['nationality'];?>">
              </div>


<div class="input-wrap col-md-12">
<textarea name="career_objective" class="form-control" placeholder="Career Objective"><?php echo $user_resume_view['career_objective'];?>
</textarea>

</div>

<div class="input-wrap col-md-12">
<textarea name="career_summary" class="form-control" placeholder="Career Summary"><?php echo $user_resume_view['career_summary'];?>
</textarea>
</div>



               <div class="input-wrap col-md-6">
                <textarea name="present_address" class="form-control" placeholder="Present Address"><?php echo $user_resume_view['present_address'];?></textarea>
                
              </div>



              <div class="input-wrap col-md-6">
                <textarea name="permanent_address" class="form-control" placeholder="Permanent Address"><?php echo $user_resume_view['permanent_address'];?></textarea>
                
              </div>


              <div class="input-wrap col-md-3">

                 <button name="personal_details" type="submit" class="btn btn-default btn-block" href="#">
              Update
            </button>
             </div>
</div>
<?php } else { ?>

Personal details dosn't added 

<?php } ?>
          </form>
     

     
      </div>
    </div>
  </div>

  <?php if ($user_edu_info_result->num_rows > 0) { // if ($user_edu == ' ') {?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
         Academic Summary 
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">

      

            <div class="controls"> 
  <form role="form" method="post" enctype="multipart/form-data" name="post"  action="edit_resume_ac.php" >
     
    <?php 
$serial = 1; 
while($user_edu_data = $user_edu_info_result->fetch_assoc()) {
 $s_num = $serial++;
$table_name = 'job_seeker_education';
?>  

 <div class="input-wrap col-md-12">
<label><?php echo 'Academic-' . $s_num;?>
  <span><a href="javascript:;" onClick="delete_function_con('<?php echo $user_edu_data['id']; ?>','<?php echo $table_name; ?>');" 
  class=""> 
    <i class="fa fa-trash-o"></i> Delete 
  </a></span>
</label>
</div>


<input type="hidden" name="id[]" value="<?php echo $user_edu_data['id'];?>" />


   <div class="entry">
                
<div class="col-md-6 input-wrap">
        <label>Level of Education</label>
         <select name="education_level[]" class="form-control" required="">
          <option value="<?php echo $user_edu_data['education_level'];?>"><?php echo $user_edu_data['education_level'];?></option>
          <option value="PSC/5 pass">PSC/5 pass</option>
          <option value="JSC/JDC/8 pass">JSC/JDC/8 pass</option>
          <option value="Secondary(SSC)">Secondary(SSC)</option>
          <option value="Higher Secondary(HSC)">Higher Secondary(HSC)</option>
          <option value="Diploma">Diploma</option>
          <option value="Bachelor/Honors">Bachelor/Honors</option>
          <option value="Masters">Masters</option>
          <option value="PhD">PhD (Doctor of Philosophy)</option>
        </select>
      </div>


    <div class="input-wrap col-md-6">
        <label>GPA/CGPA/Division</label>
         <input name="cgpa[]" placeholder="GPA/CGPA/Division" class="form-control" type="text" value="<?php echo $user_edu_data['cgpa'];?>">
      </div>




      <div class="col-md-6 input-wrap">
         <label>Year of Passing</label>
         <select name="passing_year[]" class="form-control" required="">
         <option value="<?php echo $user_edu_data['passing_year'];?>"><?php echo $user_edu_data['passing_year'];?></option>
            <?php 
           for($i = 1990 ; $i < date('Y'); $i++){
          echo "<option value='$i'>$i</option>";
           }
            ?>
          </select>
      </div>



      <div class="input-wrap col-md-6">
         <label>Concentration/Major/Group</label>
         <input name="major_group[]" placeholder="Concentration/ Major/Group" class="form-control" type="text" value="<?php echo $user_edu_data['major_group'];?>">
      </div>


      <div class="input-wrap col-md-6">
         <label>Duration (Years)</label>
         <input name="duration_years[]" placeholder="Duration (Years)" class="form-control" type="text" value="<?php echo $user_edu_data['duration_years'];?>">
      </div>


      <div class="input-wrap col-md-6">
        <label>Institute Name</label>
         <input name="institute_name[]" placeholder="Institute Name" class="form-control" type="text" value="<?php echo $user_edu_data['institute_name'];?>">
      </div><br>

<!--         <span class="input-group-btn">
              <button class="btn btn-success btn-add" type="button">
                <span class="fa fa-plus-circle"> Add Education (If Required)</span>
              </button>
          </span>
 -->

</div>



 <?php } ?>
<div class="input-wrap col-md-12">
<button name="academic_details_Update" type="submit" class="btn btn-default btn-block" href="#">
Update
</button>
</div>
</form>


   </div>
 </div>
</div>
 <?php } ?>
  </div>
  


<?php if ($profess_info_result->num_rows > 0) { ?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
       Professional Qualification 
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       




<form role="form" method="post" enctype="multipart/form-data" name="post"  action="edit_resume_ac.php" >
     
<?php  $serial = 1; 
 while($profess_info_data = $profess_info_result->fetch_assoc()) {
  $s_num = $serial++;
     $table_name = 'job_seeker_profess_info';

   ?>


<div class="input-wrap col-md-12">
<label><?php echo 'Profession-' . $s_num;?>
  <span><a href="javascript:;" onClick="delete_function_con('<?php echo $profess_info_data['id']; ?>','<?php echo $table_name; ?>');" 
  class=""> 
    <i class="fa fa-trash-o"></i> Delete 
  </a></span>
</label>
</div>



<input type="hidden" name="prof_id[]" value="<?php echo $profess_info_data['id'];?>" />








     <div class="input-wrap col-md-6">
         <label>Certification*</label>
         <input name="certification[]" placeholder="Certification" class="form-control" type="text" value="<?php echo $profess_info_data['certification'];?>">
      </div>


      <div class="input-wrap col-md-6">
        <label>Location</label>
         <input name="location[]" placeholder="Location" class="form-control" type="text" value="<?php echo $profess_info_data['location'];?>">
      </div>


      <div class="input-wrap col-md-6">
        <label>Institute*</label>
        <input name="institute[]" placeholder="Institute" class="form-control" type="text"  value="<?php echo $profess_info_data['institute'];?>">
      </div>


    <div class="input-wrap col-md-3">
        <label>Certification Period*</label>
        <input style="padding: 0" name="certi_startdate[]" placeholder="Start date" class="form-control" type="Date" value="<?php echo $profess_info_data['certi_startdate'];?>">
      </div>

        <div class="input-wrap col-md-3">
        <label>End date*</label>
        <input style="padding: 0" name="certi_enddate[]" placeholder="End date" class="form-control" type="Date" value="<?php echo $profess_info_data['certi_enddate'];?>">
      </div>

      <br>

 <?php } ?>

<div class="input-wrap col-md-12">
<button name="professional_details_Update" type="submit" class="btn btn-default btn-block" href="#">
Update
</button>
</div>
</form>





      </div>
    </div>
  </div>

   <?php } ?> 













<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading4">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
       Photograph
        </a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
      <div class="panel-body">
       


<?php if ($user_image_data['image_file'] !=''){ ?>




<form role="form" method="post" enctype="multipart/form-data" name="post"  action="edit_resume_ac.php" >



<div class="col-md-3"></div>
        


<div class="form-group">
                <label class="col-sm-2 control-label">Image Uploade :</label>
 <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                   <img src="image/<?php echo $user_image_data['image_file']; ?>" alt="Image not found">
                  </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                  <div>
                   <span class="btn btn-white btn-file">
                  <span class="fileinput-new">Select image</span>
                  <span class="fileinput-exists">Change</span>
                  
                  <input name="image_file" type="file" class="uploader" id="image_file" multiple="multiple">
                   </span>
                   <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>
                 </div>
</div>



<div class="col-md-3"></div><br>



<div class="input-wrap col-md-12">
<button name="image_Update" type="submit" class="btn btn-default btn-block" href="#">
Update
</button>
</div>


<?php }else { ?>
Image dosn't added 
<?php } ?>


</form>
</div>

  </div>







</div>

     
  </div>
   
  </div>
</div>










    </div>
  </div>
</div>







<?php include 'footer.php';?>


<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/carousel.js"></script>
<script type="text/javascript" src="js/js_script.js"></script>



  <!-- Imported scripts on this page -->
  <script src="js/fileinput.js"></script>

</body>
</html>