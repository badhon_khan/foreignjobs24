<?php
	include("fj-admin/config/confg.php");
	//@System Analyst/Programmer : Md. Saiful Islam Sagor.
	//@Author : Expert IT Solution
	//@Cell : +88 01766 40 98 19
	//@Website : www.expertitbd.com
	
	$cate_name = $_REQUEST["cate_name"];
	
	$stmt         = mysqli_query($con, 'SELECT * FROM exp_settings');
	$site_setting = mysqli_fetch_array($stmt);
	$base_url = $site_setting['base_url'];
	$title = $site_setting['title'];
	$s_com_name = $site_setting['s_com_name'];
	$s_logo = $site_setting['s_logo'];
	$m_key = $site_setting['m_key'];
	$m_desc = $site_setting['m_desc'];
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title> All Jobs |  <?php echo $title; ?> </title>
<?php include 'head.php';?>
</head>
<body>
<?php include 'header-menu.php';?>

<div class="inner-heading">
  <div class="container">
    <h3>All Jobs</h3>
  </div>
</div>
<?php  
  $c_user_id = $_REQUEST["c_user_id"];
  $recurator_info_db = mysqli_query($con, "SELECT * FROM recurator_info WHERE user_id = '$c_user_id' ");
  $recurator_info_data = mysqli_fetch_array($recurator_info_db);
  $com_logo = $recurator_info_data["com_logo"];


?>
<div class="featured_company_info_section" style="padding: 50px 0;">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="col-md-4">
          <img class="media-object" src="<?php echo $base_url; ?>job-recruiter/com-logo/<?php echo $com_logo; ?>" alt="...">
        </div>
        <div class="col-md-8">
          <h3><?php echo $recurator_info_data['company_name']; ?></h3>
          <h4><?php echo $recurator_info_data['c_type']; ?></h4>
          <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $job_location = $c_type_data["job_location"]; ?> , <?php echo $country = $recurator_info_data["country"]; ?>
        </div>
        <div class="col-md-12" style="padding-top: 50px;">
          <strong>Jobs Post By: <?php echo $recurator_info_data['company_name']; ?></strong>
          <ul class="list-group" style="padding-top: 20px;">
            <?php  
              $i = 1;
                $companey_job_list_db = mysqli_query($con, "SELECT * FROM job_listing WHERE user_id= '$c_user_id'");
                while($company_job_list_data = mysqli_fetch_array($companey_job_list_db)){
            ?>
              <li class="list-group-item">
                <strong><?php echo $i++; ?> - </strong>
                <a href="<?php echo $base_url; ?>job-details.php?job_id=<?php echo $company_job_list_data['id'] ?>"><?php echo $company_job_list_data['job_title']; ?></a>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <?php  
          $exp_settings_db = mysqli_query($con, "SELECT * FROM exp_settings WHERE id = 10 ");
          $exp_settings_data = mysqli_fetch_array($exp_settings_db);
        ?>
        <a href="<?php echo $exp_settings_data['banner_link']; ?>" target="_blank"><img src="<?php echo $base_url . "image/".$exp_settings_data['banner']; ?>" alt="banner"></a>
      </div>
    </div>
  </div>
</div>

 
<?php include 'footer.php';?>


<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/carousel.js"></script>
<script type="text/javascript" src="js/js_script.js"></script>
<div class="totop">&uarr;</div>
<style>
.totop {
    position: fixed;
    bottom: 50px;
    right: 50px;
    cursor: pointer;
    display: none;
    background: #313538;
    color: #fff;
    border-radius: 25px;
    height: 40px;
    line-height: 7px;
    padding: 15px;
    font-size: 18px;
}
</style>
<script src="<?php echo $base_url; ?>js/totop.min.js"></script>
<script>
$(document).ready(function(){
	$('.totop').tottTop();
});
</script>
</body>
</html>