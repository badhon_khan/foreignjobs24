<?php

	include("fj-admin/config/confg.php"); 
	
	// Get PHPMailer
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require 'PHPMailer/Exception.php';
	require 'PHPMailer/PHPMailer.php';
	require 'PHPMailer/SMTP.php';
	
	//@System Analyst/Programmer : Md. Saiful Islam Sagor.
	//@Author : Expert IT Solution
	//@Cell : +88 01766 40 98 19
	//@Website : www.expertitbd.com
	
	date_default_timezone_set("Asia/Dhaka");
	$date  = date("Y-m-d");
	$date_time  = date("Y-m-d g:i:s a");
	

	
	$job_id = addslashes($_REQUEST["job_id"]);
	$user_id = addslashes($_REQUEST["user_id"]);
	$job_title = addslashes($_REQUEST["job_title"]);
	
	
	$reg_action3 = mysqli_query($con,"INSERT INTO `job_apply` (`id`, `job_id`, `user_id`, `date`) VALUES (NULL, '$job_id', '$user_id', '$date')");
		
	$user_resume1 = mysqli_query($con, "SELECT * FROM job_listing WHERE id = '$job_id'");
	$recurator_info = mysqli_fetch_array($user_resume1);
	$company_name = $recurator_info["email"];
		

	/* Add PHPMailer
	################################*/
	include_once 'PHPMailer\PHPMailer.php';
	$mail = new PHPMailer();
	$mail->setFrom("badhon.officialmail@gmail.com", 'Foreignjobs');

	$mail->addAddress($company_name, "Job Apply");
	$mail->Subject = "Foreignjobs: Your Received Job Application";
	$mail->isHTML(true);
	$mail->Body = '
		<table style="width: 100%">
			<tr style="background-color: #0e0e0e; height: 200px;">
				<td style="text-align: center;"><img style="width: 300px;" src="https://codetrackers.net/foreignjobs/image/logo.png" /></td>
			</tr>
			<tr style="text-align: center; background-color: #f1f1f1">
				<td>
					<div style="padding: 70px 0;">
						<h2 style="font-family:Arial,Helvetica,sans-serif; padding-bottom: 30px;line-height: 20px;">
							Your Received Job Application <br>
							<strong>Job Name: </strong>'.$job_title.'
						</h2>
						<br><br>
						<!-- Callout Panel -->
						<p style="font-family:Arial,Helvetica,sans-serif;  padding-top: 15px;line-height: 20px;">
							If you need help or have any questions, <br /> please contact us at <a href="mailto:trade@igniteotc.com" style="font-weight: bold;color: #154462;font-family:Arial,Helvetica,sans-serif;">http://www.foreignjobs24.com/</a>
						</p><!-- /Callout Panel -->		
					</div>			
				</td>
			</tr>
		</table>
		';
	$mail->send();


?>
	
		<script>
		location.replace("apply_success.php?job_title=<?php echo $job_title; ?>");
		</script>
