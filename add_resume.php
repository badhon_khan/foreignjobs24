<?php
include("fj-admin/config/confg.php");
include("session_check.php");
$user_id=$_SESSION['user_id'];
$user_resume = "SELECT * FROM job_seeker_resume WHERE user_id = '$user_id' ";
$user_resume_data = $con->query($user_resume);


?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'head.php';?>
<link rel="stylesheet" href="css/neon-forms.css">
</head>
<body>
<?php include 'header-menu.php';?>




<div class="inner-content loginWrp">
  <div class="container">
    <div class="row">
      <?php include("user-sidebar-menu.php"); ?>
      


<div class="col-md-9"> 
  <!-- Blog List start -->
  <div class="user-content">
    <div class="panel-heading panel-heading-01"><i class="fa fa-plus-circle"></i> Add Resume</div>

    <div class="ucon-panel-body">





<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
           Personal Details 
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
         <form role="form" method="post" enctype="multipart/form-data" name="post"  action="add_resume_ac.php">


<?php if($user_resume_view = $user_resume_data->fetch_assoc()){?>
Personal details already added . If you want you can edit 
<?php } else { ?>


    <div class="input-wrap col-md-6">
         <label>Your Father's Name</label>
         <input name="fathers_ame" placeholder="Your Father's Name" class="form-control" type="text">
    </div>

    <div class="input-wrap col-md-6">
        <label>Your Mother's Name</label>
         <input name="mother_name" placeholder="Your Mother's Name" class="form-control" type="text">
    </div>


    <div class="input-wrap col-md-6">
         <label>Date of Birth*</label>
         <input name="birth_date" placeholder="Date of Birth" class="form-control" type="Date" required="">
    </div>


    <div class="input-wrap col-md-6">
         <label>Religion*</label>
         <input name="religion" placeholder="Religion" class="form-control" type="text" required="">
    </div>


     <div class="input-wrap col-md-6">
        <label>Marital Status*</label>
         <input name="marital_status" placeholder="Marital Status" class="form-control" type="text" required="">
    </div>

    <div class="input-wrap col-md-6">
         <label>Nationality*</label>
         <input name="nationality" placeholder="Nationality" class="form-control" type="text" required="">
    </div>


     <div class="input-wrap col-md-6">
       <label>Present Address*</label>
       <textarea name="present_address" class="form-control" placeholder="Present Address" required=""></textarea>
      
    </div>



<div class="input-wrap col-md-6">
<label>Permanent Address</label>
<textarea name="permanent_address" class="form-control" placeholder="Permanent Address"></textarea>

</div>


<div class="input-wrap col-md-12">
   <label>Career Objective</label>
   <textarea name="career_objective" class="form-control" placeholder="Career Objective"></textarea>
</div>

<div class="input-wrap col-md-12">
   <label>Career Summary</label>
   <textarea name="career_summary" class="form-control" placeholder="Career Summary"></textarea>
</div>


              <div class="input-wrap col-md-3">

                 <button name="personal_details" type="submit" class="btn btn-default btn-block" href="#">
              Save
            </button>
             </div>
</div>

          </form>
<?php } ?>     

     
      </div>
    </div>
  </div>







<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
         Academic Summary 
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">




<form role="form" method="post" enctype="multipart/form-data" name="post"  action="add_resume_ac.php" >
<div class="controls"> 

   <div class="entry">
                
<div class="col-md-6 input-wrap">
   <label>Level of Education*</label>
        <select name="education_level[]" class="form-control" required="">
          <option value="">Level of Education</option>
          <option value="PSC/5 pass">PSC/5 pass</option>
          <option value="JSC/JDC/8 pass">JSC/JDC/8 pass</option>
          <option value="Secondary(SSC)">Secondary(SSC)</option>
          <option value="Higher Secondary(HSC)">Higher Secondary(HSC)</option>
          <option value="Diploma">Diploma</option>
          <option value="Bachelor/Honors">Bachelor/Honors</option>
          <option value="Masters">Masters</option>
          <option value="PhD">PhD (Doctor of Philosophy)</option>
        </select>
      </div>


    <div class="input-wrap col-md-6">
       <label>GPA/CGPA/Division*</label>
        <input name="cgpa[]" placeholder="GPA/CGPA/Division" class="form-control" type="text" required="">
      </div>




      <div class="col-md-6 input-wrap">
         <label>Year of Passing*</label>
        <select name="passing_year[]" class="form-control" required="">
         <option value="">Year of Passing </option>
            <?php 
           for($i = 1990 ; $i < date('Y'); $i++){
          echo "<option value='$i'>$i</option>";
           }
            ?>
          </select>
      </div>



      <div class="input-wrap col-md-6">
         <label>Concentration/Major/Group*</label>
        <input name="major_group[]" placeholder="Concentration/Major/Group" class="form-control" type="text" required="" >
      </div>


      <div class="input-wrap col-md-6">
         <label>Duration (Years)</label>
        <input name="duration_years[]" placeholder="Duration (Years)" class="form-control" type="text" >
      </div>


      <div class="input-wrap col-md-6">
         <label>Institute Name*</label>
        <input name="institute_name[]" placeholder="Institute Name" class="form-control" type="text" required="">
      </div><br>

        <span class="input-group-btn">

              <button class="btn btn-success btn-add" type="button">
                <span class="fa fa-plus-circle"> Add Education Qualification</span>
              </button>
          </span>


</div>

      </div>
<div class="input-wrap col-md-3">
 <button name="academic_details_add" type="submit" class="btn btn-default btn-block" href="#">
Save
</button>
</div>

       </form>

      </div>
    </div>
  </div>









     
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
       Professional Qualification
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">


 
      <form role="form" method="post" enctype="multipart/form-data" name="post"  action="add_resume_ac.php" >
           <div class="controls2"> 

            <div class="entry2">


        <div class="input-wrap col-md-6">
         <label>Certification*</label>
         <input name="certification[]" placeholder="Certification" class="form-control" type="text" required="">
      </div>


      <div class="input-wrap col-md-6">
        <label>Location</label>
         <input name="location[]" placeholder="Location" class="form-control" type="text">
      </div>


      <div class="input-wrap col-md-6">
        <label>Institute*</label>
        <input name="institute[]" placeholder="Institute" class="form-control" type="text"  required="">
      </div>


    <div class="input-wrap col-md-3">
        <label>Certification Period*</label>
        <input style="padding: 0" name="certi_startdate[]" placeholder="Start date" class="form-control" type="Date" required="">
      </div>

        <div class="input-wrap col-md-3">
        <label>End date*</label>
        <input style="padding: 0" name="certi_enddate[]" placeholder="End date" class="form-control" type="Date" >
      </div>

      <br>




      <span class="input-group-btn">
              <button class="btn btn-success btn-add2" type="button">
                <span class="fa fa-plus-circle">Add Professional Qualification</span>
              </button>
          </span>
      </div>
      

      </div>



<div class="input-wrap col-md-3">
 <button name="add_professional" type="submit" class="btn btn-default btn-block" href="#">Save</button>
</div>

       </form>


      </div>
    </div>
  </div>






 
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
       Photograph
        </a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">

<?php if($user_image_data != ''){?>
Image already added . If you want you can edit 
<?php } else{ ?>
<form role="form" method="post" enctype="multipart/form-data" name="post" action="add_resume_ac.php" >



<div class="col-md-3"></div>
  
  <div class="form-group">
                <label class="col-sm-2 control-label">Image Uploade :</label>
 <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                   <!-- <img src="image/<?php echo $user_image_data['image_file']; ?>" alt="image icon not found"> -->
                  </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                  <div>
                   <span class="btn btn-white btn-file">
                  <span class="fileinput-new">Select image</span>
                  <span class="fileinput-exists">Change</span>
                  
                  <input name="image_file" type="file" class="uploader" id="image_file" multiple="multiple">
                   </span>
                   <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>
                 </div>
</div>

<div class="col-md-3"></div><br>
    

<div class="input-wrap col-md-12">
 <button name="image_add" type="submit" class="btn btn-default btn-block" href="#">Save</button>
</div>
</form>
<?php } ?>
</div>
</div>
</div>    


     
  </div>
   
  </div>
</div>







    </div>
  </div>
</div>







<?php include 'footer.php';?>


<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/carousel.js"></script>
<script type="text/javascript" src="js/js_script.js"></script>

  <!-- Imported scripts on this page -->
  <script src="js/fileinput.js"></script>


</body>
</html>